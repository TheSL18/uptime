#!/bin/bash
#Datos del desarrolador
#kevin david muñoz moreno
#tel 3025685220
#email david.munozm@protonmail.com

#Datos de version
PROGNAME=UptimeTool
NAMEPROPER=UptimeTool
VERSION=1.0.0

clear
#Funcion de salida
function ctrl_c(){
    echo -e "\n[!] Saliendo...\n"
    exit 1
}

#Orden ctrl+c
trap ctrl_c INT

#Banner
echo "██╗░░░██╗██████╗░████████╗██╗███╗░░░███╗███████╗████████╗░█████╗░░█████╗░██╗░░░░░"
echo "██║░░░██║██╔══██╗╚══██╔══╝██║████╗░████║██╔════╝╚══██╔══╝██╔══██╗██╔══██╗██║░░░░░"
echo "██║░░░██║██████╔╝░░░██║░░░██║██╔████╔██║█████╗░░░░░██║░░░██║░░██║██║░░██║██║░░░░░"
echo "██║░░░██║██╔═══╝░░░░██║░░░██║██║╚██╔╝██║██╔══╝░░░░░██║░░░██║░░██║██║░░██║██║░░░░░"
echo "╚██████╔╝██║░░░░░░░░██║░░░██║██║░╚═╝░██║███████╗░░░██║░░░╚█████╔╝╚█████╔╝███████╗"
echo "░╚═════╝░╚═╝░░░░░░░░╚═╝░░░╚═╝╚═╝░░░░░╚═╝╚══════╝░░░╚═╝░░░░╚════╝░░╚════╝░╚══════╝"
sleep 1
echo ""
echo "█▄▄ █▄█   █▀▄▀█ █▀█ ░ █░█ ▄▀█ █▀▀ █▄▀ █▀▀ █▀█"
echo "█▄█ ░█░   █░▀░█ █▀▄ ▄ █▀█ █▀█ █▄▄ █░█ ██▄ █▀▄"
echo ""
sleep 1

#Definicion de la herramienta
echo "Herramienta de automatizacion para la obtencion de tiempo de caida para las aplicaciones de seguros bolivar"
echo "[+] Presione 1 para calcular por porcentaje "
echo "[+] Presione 2 para calcular por horas "
echo "Ingresa el tipo de calculo -> "

#Lee la variable ingresada para acceder al menú
read opcion

#Usa case para encontrar un match en el menu de opciones y proceder con la accion
case $opcion in

#Opciones
1)
echo "[+] Ingresa el procentaje para calcular la indisponibilidad en horas (ejemplo 99.9): "
read pct
curl -s https://get.uptime.is/api?sla="$pct"
sleep 1
echo ""
echo "Gracias por utilizar esta herramienta"
;;

2)
echo "[+] Ingresa las horas para calcular la indisponibilidad en porcentaje (ejemplo 1h20m): "
read hrs
curl -s https://get.uptime.is/api?down="$hrs"
sleep 1
echo ""
echo "Gracias por utilzar esta herramienta"
;;

esac
